import React from 'react';
import './Welcome.scss';

import Text from '../../atoms/text/Text';

import CwSvg from '../../../../public/res/svg/coldwire.svg';

function Welcome() {
  return (
    <div className="app-welcome flex--center">
      <div className="app-welcome__card">
        <img className="app-welcome__logo noselect" src={CwSvg} alt="Coldwire logo" />
        <Text className="app-welcome__heading" variant="h1" weight="medium" primary>Welcome to Coldwire</Text>
        <Text className="app-welcome__subheading" variant="s1">Organize safely, build revolutions</Text>
        <Text className="app-welcome__subheading" variant="b1">Using <a href="https://cinny.in" target="_blank">Cinny</a> and <a href="https://matrix.org/" target="_blank">Matrix</a> </Text>
      </div>
    </div>
  );
}

export default Welcome;
